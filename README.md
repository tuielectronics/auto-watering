# auto watering

> DIY your own auto watering project step by step

## 1.  Bill of Materials
- mini water pump module x 1
- ESP8266 development board
- Moisture sensor
- Power bank DIY kit
- micro USB cable
- 18650 battery x 1
- Jump wires
- Spring water empty bottle

## 2.  Tools
- Paper knife
- Glue gun & sticks
- Your plant